﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeMachine.Core;

namespace TimeMachine.Providers
{
	/// <summary>
	/// Allows modify results of another <see cref="ITimeProvider"/>.
	/// </summary>
	public sealed class ModifiedTimeProvider : ITimeProvider {
		private readonly ITimeProvider originalProvider;
		private readonly Func<DateTime, DateTime> modification;

		public DateTime GetNow() {
			var originalResult = this.originalProvider.GetNow();
			var result = this.modification(originalResult);
			return result;
		}
		public DateTime GetUtcNow() {
			var originalResult = this.originalProvider.GetUtcNow();
			var result = this.modification(originalResult);
			return result;
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="originalProvider">Time provider used as source of results modified by <paramref name="modification"/>.</param>
		/// <param name="modification">Function used to modify results from <paramref name="originalProvider"/>.</param>
		public ModifiedTimeProvider(ITimeProvider originalProvider, Func<DateTime, DateTime> modification) {
			this.originalProvider = originalProvider ?? throw new ArgumentNullException(nameof(originalProvider));
			this.modification = modification ?? throw new ArgumentNullException(nameof(modification));
		}

		/// <summary>
		/// Constructor.
		/// Will use <see cref="SystemTimeProvider"/> for results passing to <paramref name="modification"/>.
		/// </summary>
		/// <param name="modification">Function used to modify results from <see cref="SystemTimeProvider"/>.</param>
		public ModifiedTimeProvider(Func<DateTime, DateTime> modification) {
			this.originalProvider = SystemTimeProvider.GetDefaultOrThrow();
			this.modification = modification ?? throw new ArgumentNullException(nameof(modification));
		}
	}
}
