﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeMachine.Core;

namespace TimeMachine.Providers
{
	/// <summary>
	/// Implementation of <see cref="ITimeProvider"/> by passed delegates.
	/// </summary>
	/// <remarks>
	/// Appropriate for providing custom time.
	/// </remarks>
	public sealed class DelegatedTimeProvider : ITimeProvider {
		private readonly Func<DateTime> now;
		public DateTime GetNow() => this.now();

		private readonly Func<DateTime> utcNow;
		public DateTime GetUtcNow() => this.utcNow();

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="nowFunc">Function used by <see cref="GetNow()"/>. The <see cref="GetUtcNow()"/> will use that also via <see cref="DateTime.ToUniversalTime()"/> method.</param>
		public DelegatedTimeProvider(Func<DateTime> nowFunc) {
			this.now = nowFunc ?? throw new ArgumentNullException(nameof(nowFunc));
			this.utcNow = () => nowFunc().ToUniversalTime();
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="nowFunc">Function used by <see cref="GetNow()"/>.</param>
		/// <param name="utcNowFunc">Function used by <see cref="GetUtcNow()"/>.</param>
		public DelegatedTimeProvider(Func<DateTime> nowFunc, Func<DateTime> utcNowFunc) {
			this.now = nowFunc ?? throw new ArgumentNullException(nameof(nowFunc));
			this.utcNow = utcNowFunc ?? throw new ArgumentNullException(nameof(utcNowFunc));
		}
	}
}
