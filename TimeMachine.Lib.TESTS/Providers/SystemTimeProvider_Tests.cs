﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TimeMachine.Providers
{
	[TestClass]
	public class SystemTimeProvider_Tests {
		[TestMethod]
		public void Now_Returns_CorrectValues() {
			var provider = SystemTimeProvider.Default;

			const int LOOPS_COUNT = 10;
			const int VALUES_COUNT = 50;
			for (var i = 0; i < LOOPS_COUNT; i++) {
				var waitTime = TimeSpan.FromMilliseconds(i * 2);

				var systemValues = new DateTime[VALUES_COUNT + 1];
				var providerValues = new DateTime[VALUES_COUNT];
				for (var k = 0; k < VALUES_COUNT; k++) {
					systemValues[k] = DateTime.Now;
					TestHelper.BusyWaitUsingDateTimeNow(waitTime);
					providerValues[k] = provider.GetNow();
					systemValues[k + 1] = DateTime.Now;
				}

				for (var k = 0; k < VALUES_COUNT; k++) {
					var systemValue0 = systemValues[k];
					var providerValue = providerValues[k];
					var systemValue1 = systemValues[k + 1];

					var minProviderValue = systemValue0 + waitTime;

					Assert.IsTrue(providerValue >= minProviderValue, $"The providerValue ({providerValue.Ticks}) is equal or greater than minimal acceptable value ({minProviderValue.Ticks}).");
					Assert.IsTrue(providerValue <= systemValue1, $"The providerValue ({providerValue.Ticks}) is equal or lower than maximal acceptable value ({systemValue1.Ticks}).");
				}
			}

			return;
		}

		[TestMethod]
		public void UtcNow_Returns_CorrectValues() {
			var provider = SystemTimeProvider.Default;

			const int LOOPS_COUNT = 10;
			const int VALUES_COUNT = 50;
			for (var i = 0; i < LOOPS_COUNT; i++) {
				var waitTime = TimeSpan.FromMilliseconds(i * 2);

				var systemValues = new DateTime[VALUES_COUNT + 1];
				var providerValues = new DateTime[VALUES_COUNT];
				for (var k = 0; k < VALUES_COUNT; k++) {
					systemValues[k] = DateTime.UtcNow;
					TestHelper.BusyWaitUsingDateTimeNow(waitTime);
					providerValues[k] = provider.GetUtcNow();
					systemValues[k + 1] = DateTime.UtcNow;
				}

				for (var k = 0; k < VALUES_COUNT; k++) {
					var systemValue0 = systemValues[k];
					var providerValue = providerValues[k];
					var systemValue1 = systemValues[k + 1];

					var minProviderValue = systemValue0 + waitTime;

					Assert.IsTrue(providerValue >= minProviderValue, $"The providerValue ({providerValue.Ticks}) is equal or greater than minimal acceptable value ({minProviderValue.Ticks}).");
					Assert.IsTrue(providerValue <= systemValue1, $"The providerValue ({providerValue.Ticks}) is equal or lower than maximal acceptable value ({systemValue1.Ticks}).");
				}
			}

			return;
		}
	}
}
