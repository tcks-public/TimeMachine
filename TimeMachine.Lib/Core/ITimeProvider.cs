﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeMachine.Core {
	/// <summary>
	/// Interface for provider of current time.
	/// </summary>
	public interface ITimeProvider {
		/// <summary>
		/// Returns current time in current time zone.
		/// </summary>
		/// <returns></returns>
		DateTime GetNow();

		/// <summary>
		/// Returns current time as UTC.
		/// </summary>
		/// <returns></returns>
		DateTime GetUtcNow();
	}
}
