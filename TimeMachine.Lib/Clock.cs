﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeMachine.Core;

namespace TimeMachine {
	/// <summary>
	/// Provides current time regarding to configuration.
	/// </summary>
	public static class Clock {
		/// <summary>
		/// Returns current time in current time zone regarding to current configuration of time provider (<see cref="TimeProvider.TimeProvider"/>).
		/// </summary>
		/// <returns></returns>
		public static DateTime GetNow() => ClockConfig.nowFunc();

		/// <summary>
		/// Returns current time in current time zone regarding to current configuration of time provider (<see cref="TimeProvider.TimeProvider"/>).
		/// </summary>
		public static DateTime Now => ClockConfig.nowFunc();

		/// <summary>
		/// Returns current time as UTC regarding to current configuration of time provider (<see cref="TimeProvider.TimeProvider"/>).
		/// </summary>
		/// <returns></returns>
		public static DateTime GetUtcNow() => ClockConfig.utcNowFunc();

		/// <summary>
		/// Returns current time as UTC regarding to current configuration of time provider (<see cref="TimeProvider.TimeProvider"/>).
		/// </summary>
		public static DateTime UtcNow => ClockConfig.utcNowFunc();
	}
}
