﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeMachine.Providers;

namespace TimeMachine.Core
{
	[TestClass]
	public class ClockConfig_Tests {
		[TestMethod]
		public void ResetProvider_Assigns_SystemTimeProvider() {
			ClockConfig.Provider = new ConstantTimeProvider(DateTime.Now);
			ClockConfig.ResetProvider();
			Assert.IsInstanceOfType(ClockConfig.Provider, typeof(SystemTimeProvider));
		}

		[TestMethod]
		public void Provider_CanNotBeAssignedToNull() {
			Exception thrownException = null;
			try { ClockConfig.Provider = null; }
			catch (Exception exc) { thrownException = exc; }

			Assert.IsInstanceOfType(thrownException, typeof(ArgumentNullException));
		}
	}
}
