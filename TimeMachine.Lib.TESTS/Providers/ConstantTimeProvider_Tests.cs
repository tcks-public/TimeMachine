﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TimeMachine.Providers
{
	[TestClass]
	public class ConstantTimeProvider_Tests {
		[TestMethod]
		public void Now_Returns_CorrectValues() {
			var expected = new DateTime(2017, 10, 25, 14, 35, 54);
			var provider = new ConstantTimeProvider(expected);

			Assert.AreEqual(expected, provider.GetNow());
			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(1));
			Assert.AreEqual(expected, provider.GetNow());
			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(2));
			Assert.AreEqual(expected, provider.GetNow());
		}

		[TestMethod]
		public void UtcNow_Returns_CorrectValues() {
			var expected = new DateTime(2017, 10, 25, 14, 35, 54, DateTimeKind.Local);
			var provider = new ConstantTimeProvider(expected);

			Assert.AreEqual(expected.ToUniversalTime(), provider.GetUtcNow());
			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(1));
			Assert.AreEqual(expected.ToUniversalTime(), provider.GetUtcNow());
			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(2));
			Assert.AreEqual(expected.ToUniversalTime(), provider.GetUtcNow());
		}
	}
}
