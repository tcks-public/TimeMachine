﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeMachine
{
	/// <summary>
	/// Contains helper members used in tests.
	/// </summary>
	public static class TestHelper {
		/// <summary>
		/// Will run in loop at least for <paramref name="waitTime"/> measured by <see cref="DateTime.Now"/>.
		/// </summary>
		/// <param name="waitTime">Minimum time taken by this method.</param>
		public static void BusyWaitUsingDateTimeNow(TimeSpan waitTime) {
			var start = DateTime.Now;
			while (true) {
				var now = DateTime.Now;
				var diff = now - start;
				if (diff >= waitTime) {
					return;
				}
			}
		}
	}
}
