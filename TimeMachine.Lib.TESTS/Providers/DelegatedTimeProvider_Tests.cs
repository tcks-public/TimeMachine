﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TimeMachine.Providers
{
	[TestClass]
	public class DelegatedTimeProvider_Tests {
		[TestMethod]
		public void Now_Returns_CorrectValues() {
			var expected = new DateTime(2017, 10, 25, 14, 35, 54);
			var provider = new DelegatedTimeProvider(() => expected);

			Assert.AreEqual(expected, provider.GetNow());
			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(1));
			Assert.AreEqual(expected, provider.GetNow());

			expected = expected.AddMonths(-1);
			Assert.AreEqual(expected, provider.GetNow());
			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(1));
			Assert.AreEqual(expected, provider.GetNow());
		}

		[TestMethod]
		public void UtcNow_DefinedImplicitly_Returns_CorrectValues() {
			var expected = new DateTime(2017, 10, 25, 14, 35, 54);
			var provider = new DelegatedTimeProvider(() => expected);

			Assert.AreEqual(expected.ToUniversalTime(), provider.GetUtcNow());
			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(1));
			Assert.AreEqual(expected.ToUniversalTime(), provider.GetUtcNow());

			expected = expected.AddMonths(-1);
			Assert.AreEqual(expected.ToUniversalTime(), provider.GetUtcNow());
			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(1));
			Assert.AreEqual(expected.ToUniversalTime(), provider.GetUtcNow());
		}

		[TestMethod]
		public void UtcNow_DefinedExplicitly_Returns_CorrectValues() {
			var expected = new DateTime(2017, 10, 25, 14, 35, 54);
			var utcExpected = expected.ToUniversalTime().AddYears(5);
			var provider = new DelegatedTimeProvider(() => expected, () => utcExpected);

			Assert.AreEqual(utcExpected, provider.GetUtcNow());
			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(1));
			Assert.AreEqual(utcExpected, provider.GetUtcNow());

			utcExpected = utcExpected.AddMonths(-1);
			Assert.AreEqual(utcExpected, provider.GetUtcNow());
			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(1));
			Assert.AreEqual(utcExpected, provider.GetUtcNow());
		}
	}
}
