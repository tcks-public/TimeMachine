﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeMachine.Core;

namespace TimeMachine.Providers
{
	/// <summary>
	/// Implementation of <see cref="ITimeProvider"/> with static members from <see cref="DateTime"/>.
	/// </summary>
	/// <remarks>
	/// This class is:
	///  - stateless
	///  - thread-safe (both instance & static members)
	/// </remarks>
	public sealed class SystemTimeProvider : ITimeProvider {
		#region Singleton
		internal static readonly SystemTimeProvider @default = new SystemTimeProvider();

		/// <summary>
		/// Prepared instance of <see cref="SystemTimeProvider"/>.
		/// </summary>
		public static SystemTimeProvider Default => @default;

		/// <summary>
		/// Returns prepared instance of <see cref="SystemTimeProvider"/>.
		/// If instance is not available (its major error) the <see cref="InvalidOperationException"/> is thrown.
		/// </summary>
		/// <returns></returns>
		public static SystemTimeProvider GetDefaultOrThrow() => @default ?? throw new InvalidOperationException("The singleton instance is not set (is null).");
		#endregion Singleton

		/// <summary>
		/// Returns value from <see cref="DateTime.Now"/>.
		/// </summary>
		/// <returns></returns>
		public DateTime GetNow() => DateTime.Now;

		/// <summary>
		/// Returns value from <see cref="DateTime.UtcNow"/>.
		/// </summary>
		/// <returns></returns>
		public DateTime GetUtcNow() => DateTime.UtcNow;
	}
}
