﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeMachine.Core;
using TimeMachine.Providers;

namespace TimeMachine
{
	[TestClass]
	public class Clock_Tests {
		[TestInitialize]
		public void TestInit() {
			ClockConfig.ResetProvider();
		}

		[TestCleanup]
		public void TestCleanup() {
			ClockConfig.ResetProvider();
		}

		[TestMethod]
		public void GetNow_IsCalling_TimeProvider_Provider_GetNow() {
			var nowCounter = 0;
			var nowValue = DateTime.Now;

			var utcCounter = 0;
			var utcValue = DateTime.Now.AddYears(5);

			var myProvider = new DelegatedTimeProvider(() => {
				nowCounter++;
				return nowValue;
			}, () => {
				utcCounter++;
				return utcValue;
			});

			ClockConfig.Provider = myProvider;

			Assert.AreEqual(0, nowCounter);
			Assert.AreEqual(0, utcCounter);

			Assert.AreEqual(nowValue, Clock.GetNow());
			Assert.AreEqual(1, nowCounter);

			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(1));

			Assert.AreEqual(nowValue, Clock.GetNow());
			Assert.AreEqual(2, nowCounter);

			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(2));

			Assert.AreEqual(nowValue, Clock.GetNow());
			Assert.AreEqual(3, nowCounter);
		}

		[TestMethod]
		public void Now_IsCalling_TimeProvider_Provider_GetNow() {
			var nowCounter = 0;
			var nowValue = DateTime.Now;

			var utcCounter = 0;
			var utcValue = DateTime.Now.AddYears(5);

			var myProvider = new DelegatedTimeProvider(() => {
				nowCounter++;
				return nowValue;
			}, () => {
				utcCounter++;
				return utcValue;
			});

			ClockConfig.Provider = myProvider;

			Assert.AreEqual(0, nowCounter);
			Assert.AreEqual(0, utcCounter);

			Assert.AreEqual(nowValue, Clock.GetNow());
			Assert.AreEqual(1, nowCounter);

			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(1));

			Assert.AreEqual(nowValue, Clock.Now);
			Assert.AreEqual(2, nowCounter);

			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(2));

			Assert.AreEqual(nowValue, Clock.Now);
			Assert.AreEqual(3, nowCounter);
		}

		[TestMethod]
		public void GetUtcNow_IsCalling_TimeProvider_Provider_GetUtcNow() {
			var nowCounter = 0;
			var nowValue = DateTime.Now;

			var utcCounter = 0;
			var utcValue = DateTime.Now.AddYears(5);

			var myProvider = new DelegatedTimeProvider(() => {
				nowCounter++;
				return nowValue;
			}, () => {
				utcCounter++;
				return utcValue;
			});

			ClockConfig.Provider = myProvider;

			Assert.AreEqual(0, nowCounter);
			Assert.AreEqual(0, utcCounter);

			Assert.AreEqual(utcValue, Clock.GetUtcNow());
			Assert.AreEqual(1, utcCounter);

			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(1));

			Assert.AreEqual(utcValue, Clock.GetUtcNow());
			Assert.AreEqual(2, utcCounter);

			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(2));

			Assert.AreEqual(utcValue, Clock.GetUtcNow());
			Assert.AreEqual(3, utcCounter);
		}

		[TestMethod]
		public void UtcNow_IsCalling_TimeProvider_Provider_GetUtcNow() {
			var nowCounter = 0;
			var nowValue = DateTime.Now;

			var utcCounter = 0;
			var utcValue = DateTime.Now.AddYears(5);

			var myProvider = new DelegatedTimeProvider(() => {
				nowCounter++;
				return nowValue;
			}, () => {
				utcCounter++;
				return utcValue;
			});

			ClockConfig.Provider = myProvider;

			Assert.AreEqual(0, nowCounter);
			Assert.AreEqual(0, utcCounter);

			Assert.AreEqual(utcValue, Clock.UtcNow);
			Assert.AreEqual(1, utcCounter);

			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(1));

			Assert.AreEqual(utcValue, Clock.UtcNow);
			Assert.AreEqual(2, utcCounter);

			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(2));

			Assert.AreEqual(utcValue, Clock.UtcNow);
			Assert.AreEqual(3, utcCounter);
		}
	}
}
