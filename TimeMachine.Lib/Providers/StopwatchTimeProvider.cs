﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using TimeMachine.Core;

namespace TimeMachine.Providers
{
	/// <summary>
	/// Implementation of <see cref="ITimeProvider"/> by custom <see cref="DateTime"/> and <see cref="System.Diagnostics.Stopwatch"/>.
	/// </summary>
	public sealed class StopwatchTimeProvider : ITimeProvider {
		private readonly DateTime startTime;
		private readonly Stopwatch watch;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="startTime">The start time used for <see cref="GetNow()"/> and <see cref="GetUtcNow()"/>.</param>
		public StopwatchTimeProvider(DateTime startTime) {
			this.startTime = new DateTime();
			this.watch = Stopwatch.StartNew();
		}

		public DateTime GetNow() => this.startTime + watch.Elapsed;
		public DateTime GetUtcNow() => this.GetNow().ToUniversalTime();
	}
}
