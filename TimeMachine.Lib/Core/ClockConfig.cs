﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeMachine.Providers;

namespace TimeMachine.Core {
	/// <summary>
	/// Configuration for <see cref="Clock"/>.
	/// </summary>
	public static class ClockConfig {
		#region Providers
		/// <summary>
		/// Current time provider.
		/// </summary>
		private static ITimeProvider timeProvider;

		/// <summary>
		/// Cached delegate for geting current time.
		/// </summary>
		internal static Func<DateTime> nowFunc;

		/// <summary>
		/// Cached delegate for geting current time in utc.
		/// </summary>
		internal static Func<DateTime> utcNowFunc;

		/// <summary>
		/// The current provider of time.
		/// </summary>
		public static ITimeProvider Provider {
			get { return timeProvider; }
			set {
				if (value is null) { throw new ArgumentNullException(nameof(value)); }

				timeProvider = value;
				nowFunc = value.GetNow;
				utcNowFunc = value.GetUtcNow;
			}
		}

		/// <summary>
		/// Resets <see cref="Provider"/> to <see cref="SystemTimeProvider"/>.
		/// </summary>
		public static void ResetProvider() {
			Provider = SystemTimeProvider.GetDefaultOrThrow();
		}
		#endregion Providers

		/// <summary>
		/// Static constructor.
		/// </summary>
		static ClockConfig() {
			ResetProvider();
		}
	}
}
