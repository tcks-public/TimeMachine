# TimeMachine

Make source code depending on current time great again!

## Description

TimeMachine is microscopic library solving dependency on current time.
Make your code testable and more reliable than ever before.
Stop using `DateTime.Now` and lets start with TimeMachine!

## Examples

### Getting current time.

`DateTime currentTime = Clock.Now;`

### Having constant time.

```csharp
// now the time will be still 2017-01-01 12:30
TimeMachine.Core.ClockConfig.Provider = new ConstantTimeProvider(new DateTime(2017, 01, 01, 12, 30, 0));
```

### Reseting current time to system time.

```csharp
// now the results of Clock.Now and DateTime.Now will be same
TimeMachine.Core.ClockConfig.ResetProvider();
```

### Pretending the code started on 2017-01-01 12:30.

```csharp
// now the time will still run, but the relative point is set to 2017-01-01 12:30
TimeMachine.Core.ClockConfig.Provider = new StopwatchTimeProvider(new DateTime(2017, 01, 01, 12, 30, 0));
```

### Using custom time function.

```csharp
// now the time will queried from your custom function
TimeMachine.Core.ClockConfig.Provider = new DelegatedTimeProvider(()=> MyClass.MyCurrentTime());
```

### Modifying current time.

```csharp
// now the time will be modified by 5 minutes
TimeMachine.Core.ClockConfig.Provider = new ModifiedTimeProvider((t) => t.AddMinutes(5));
```