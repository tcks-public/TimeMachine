﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TimeMachine.Providers
{
	[TestClass]
	public class StopwatchTimeProvider_Tests {
		[TestMethod]
		public void Now_Returns_CorrectValues() {
			var startTime = DateTime.Now;
			var provider = new StopwatchTimeProvider(startTime);
			var swatch = System.Diagnostics.Stopwatch.StartNew();

			const int LOOPS_COUNT = 10;
			const int VALUES_COUNT = 50;
			for (var i = 0; i < LOOPS_COUNT; i++) {
				var waitTime = TimeSpan.FromMilliseconds(i * 2);

				for (var k = 0; k < VALUES_COUNT; k++) {
					var providerValue = provider.GetNow();
					var maxValue = startTime + swatch.Elapsed;
					TestHelper.BusyWaitUsingDateTimeNow(waitTime);

					Assert.IsTrue(providerValue <= maxValue, $"The providerValue ({providerValue.Ticks}) is equal or lower than maximal acceptable value ({maxValue.Ticks}).");
				}
			}

			return;
		}

		[TestMethod]
		public void UtcNow_Returns_CorrectValues() {
			var startTime = DateTime.Now;
			var provider = new StopwatchTimeProvider(startTime);
			var swatch = System.Diagnostics.Stopwatch.StartNew();

			const int LOOPS_COUNT = 10;
			const int VALUES_COUNT = 50;
			for (var i = 0; i < LOOPS_COUNT; i++) {
				var waitTime = TimeSpan.FromMilliseconds(i * 2);

				for (var k = 0; k < VALUES_COUNT; k++) {
					var providerValue = provider.GetUtcNow();
					var maxValue = startTime.ToUniversalTime() + swatch.Elapsed;
					TestHelper.BusyWaitUsingDateTimeNow(waitTime);

					Assert.IsTrue(providerValue <= maxValue, $"The providerValue ({providerValue.Ticks}) is equal or lower than maximal acceptable value ({maxValue.Ticks}).");
				}
			}

			return;
		}
	}
}
