﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeMachine.Core;

namespace TimeMachine.Providers
{
	/// <summary>
	/// Implementation of <see cref="ITimeProvider"/> by constant value of <see cref="DateTime"/>.
	/// </summary>
	public sealed class ConstantTimeProvider : ITimeProvider {
		private readonly DateTime value;
		/// <summary>
		/// Returns value used for <see cref="GetNow()"/> and <see cref="GetUtcNow()"/>.
		/// </summary>
		public DateTime Value => this.value;

		public DateTime GetNow() => value;
		public DateTime GetUtcNow() => value.ToUniversalTime();

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="value">Value used for <see cref="GetNow()"/> and <see cref="GetUtcNow()"/>.</param>
		public ConstantTimeProvider(DateTime value) {
			this.value = value;
		}
	}
}
