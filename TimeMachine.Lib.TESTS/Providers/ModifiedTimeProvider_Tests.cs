﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TimeMachine.Providers
{
	[TestClass]
	public class ModifiedTimeProvider_Tests {
		[TestMethod]
		public void GetNow_Returns_Modified_Results() {
			var origProvider = new ConstantTimeProvider(DateTime.Now);
			var testProvider = new ModifiedTimeProvider(origProvider, dtm => dtm.AddDays(5));

			{
				var origValue = origProvider.GetNow();
				var testValue = testProvider.GetNow();
				Assert.AreEqual(TimeSpan.FromDays(5), testValue - origValue);
			}

			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(1));

			{
				var origValue = origProvider.GetNow();
				var testValue = testProvider.GetNow();
				Assert.AreEqual(TimeSpan.FromDays(5), testValue - origValue);
			}

			TestHelper.BusyWaitUsingDateTimeNow(TimeSpan.FromSeconds(2));

			{
				var origValue = origProvider.GetNow();
				var testValue = testProvider.GetNow();
				Assert.AreEqual(TimeSpan.FromDays(5), testValue - origValue);
			}
		}
	}
}
